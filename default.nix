let
  pkgs = import <nixpkgs> {};
in
{ stdenv ? pkgs.stdenv }:

pkgs.python3Packages.buildPythonPackage {
  name = "drae";
  version = "0.2.0";
  src = if pkgs.lib.inNixShell then null else ./.;
  propagatedBuildInputs = with pkgs;
   [ python3
     python3Packages.lxml
     python3Packages.beautifulsoup4
     python3Packages.requests2
     python3Packages.future ];
}
